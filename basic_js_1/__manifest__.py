# -*- coding: utf-8 -*-

{
    'name': "Odoo JS Basic",

    'summary': """
        Odoo JS Basic""",

    'description': """
        Create display notification popup
    """,

    'author': "Duy Anh",
    'website': "ahd.dev.backend@gmail.com",

    'category': 'Uncategorized',
    'version': '15.0.0.1',
    'license': 'LGPL-3',

    # any module necessary for this one to work correctly
    'depends': [
        'base', 'web', 'hr', 'crm',
    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',

        'views/odoo_js_one_views.xml',

        # 'data/helpdesk_sequence_data.xml',

        'security/ir.model.access.csv',

        # 'wizard/hr_plan_wizard_views.xml',

        'menu/menu.xml',
    ],
    'assets': {
        'web.assets_qweb': [
            # 'crnd_web_diagram_plus/static/src/xml/base_diagram.xml',
        ],
        'web.assets_backend': [
            # 'crnd_web_diagram_plus/static/src/scss/diagram_view.scss',

            # 'crnd_web_diagram_plus/static/src/js/vec2.js',
            'basic_js_1/static/src/js/popup.js',
            
        ],
        'web.qunit_suite_tests': [
            # 'crnd_web_diagram_plus/static/tests/diagram_tests.js',
        ],
    },
    'installable': True,
}
