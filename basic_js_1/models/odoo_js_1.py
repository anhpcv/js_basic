from odoo import models, fields


class OdooJS1(models.Model):
    _name = 'odoo.js.one'
    _description = 'Odoo JS 1'

    name = fields.Char('Name')
    num_videos = fields.Integer('Number Videos')
