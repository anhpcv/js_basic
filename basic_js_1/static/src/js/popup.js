odoo.define('basic_js_1.odoo_js_warning', function (require){
    'use strict'

    console.log('odoo popup js');

    var core = require("web.core");
    var _t = core._t;
    var FormController = require('web.FormController');

    var ExtendFormController = FormController.include({
        saveRecord : function(){
            console.log("Save record now!!!!!!!!!!");
            var res = this._super.apply(this, arguments);
            console.log("res     ///////////////////", res);

            // This parseURLParams method is copied on internet
            function parseURLParams(url) {
                var queryStart = url.indexOf("?") + 1,
                    queryEnd = url.indexOf("#") + 1 || url.length + 1,
                    query = url.slice(queryStart, queryEnd - 1),
                    pairs = query.replace(/\+/g, " ").split("&"),
                    parms = {}, i, n, v, nv;

                if (query === url || query === "") return;

                for (i = 0; i < pairs.length; i++) {
                    nv = pairs[i].split("=", 2);
                    n = decodeURIComponent(nv[0]);
                    v = decodeURIComponent(nv[1]);

                    if (!parms.hasOwnProperty(n)) parms[n] = [];
                    parms[n].push(nv.length === 2 ? v : null);
                }
                return parms;
            }

            if (this.modelName == 'odoo.js.one'){
                // Notify warning when click save button

                var self = this;
                console.log("self     ///////////////////", self);

                var url = window.location.href;
                console.log("url     ///////////////////", url);

                var page_url = url.replace('#', '?');
                console.log("page_url        //////////////////", page_url);

                var params = parseURLParams(page_url);
                console.log("params        //////////////////", params);

                var num_videos = document.querySelector('input[name="num_videos"]').value
                console.log("num_videos        //////////////////", num_videos);

                if (num_videos < 500){
                    self.displayNotification({
                        type: "warning",
                        title: _t("Notify Message"),
                        message: _t("Number of Videos must be greater than 500."),
                    });
                }

                if(params?.id && params){
                    self._rpc({
                        model: 'odoo.js.one',
                        method: 'search_read',
                        fields: ['name', 'num_videos'],
                        context: self.context,
                        domain: [['id', '=', params.id[0]]],
                    })
                    .then(
                        function(result){
                            console.log('result  1111    ////////////', result);
                            return new Promise(function(resolve, reject){
                                if (num_videos < 500){
                                    var vals = {
                                        'result': result,
                                        'msg' : 'Number of Videos must be greater than 500'
                                    }
                                    reject(vals);
                                }
                                else {
                                    self.displayNotification({
                                        type: "success",
                                        title: _t("Notify Message"),
                                        message: _t("Record is saved."),
                                    });
                                }
                            })
                        }
                    )
                    .catch((vals) => {
                        self.displayNotification({
                            type: "warning",
                            title: _t("Notify Message"),
                            message: _t(vals?.msg),
                        });
                    })
                }
            }
            return res
        }
    })
})
