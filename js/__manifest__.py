# -*- coding: utf-8 -*-
{
    'name': "OWL JS Basic",

    'summary': """
        OWL JS Basic""",

    'description': """
        OWL JS Basic
    """,

    'author': "",
    'website': "",

    'category': 'Uncategorized',
    'version': '15.0.0.1',
    'license': 'LGPL-3',

    # any module necessary for this one to work correctly
    'depends': [
        'base', 'web'
    ],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',

        # 'data/helpdesk_category_data.xml',

        # 'views/invoice_category_views.xml',

        # 'menu/menu.xml',
        'menu/menu_data.xml',
    ],

    'assets': {
        'web.assets_qweb': [
            'js/static/template/demo_owl_template.xml',
        ],
        'web.assets_backend': [
            'js/static/src/demo/demo_action.js',
        ],
    },
}
