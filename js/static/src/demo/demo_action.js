odoo.define('js.demo_owl_js', function (require) {
"use strict";

var AbstractAction = require('web.AbstractAction');
var core = require('web.core');

const DemoAction = AbstractAction.extend({
    contentTemplate: 'demo_owl.template'
});

core.action_registry.add('demo_owl_view', DemoAction);

return DemoAction;

});
